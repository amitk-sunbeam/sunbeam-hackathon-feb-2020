const express = require('express');
const router = express.Router();
const Blog = require('../models/Blog')
const utils = require('../utils')
const multer = require('multer')
const upload = multer({dest: 'public'})


router.get('/details/:id', (request, response) => {
    const id = request.params.id

    Blog.findOne({_id: id}, (error, blog) => {
        if (error) {
            response.send(utils.createResult('Blog doesnot exist'))
        }
        response.send(utils.createResult(error, blog))
    })
})

router.get('/public', (request, response) => {
    Blog.find({public: true, deleted: false})
        .exec((error, blogs) => {
            response.send(utils.createResult(error, blogs))
        })
})

router.get('/all', (request, response) => {
    Blog.find({deleted: false})
        .exec((error, blogs) => {
            response.send(utils.createResult(error, blogs))
        })
})

router.post('/search', (request, response) => {
    const {text} = request.body
    Blog.find({
            public: true,
            deleted: false, 
            $or:[
                {title: new RegExp(text, 'i')},
                {body: new RegExp(text, 'i')},
            ]
        })
        .exec((error, blogs) => {
            response.send(utils.createResult(error, blogs))
        })
})

router.get('/shared-with-me/:id', (request, response) => {
    const id = request.params.id

    Blog.find({sharedWith: id, deleted: false})
        .exec((error, blogs) => {
            response.send(utils.createResult(error, blogs))
        })
})


router.get('/my/:id', (request, response) => {
    const id = request.params.id
    Blog.find({author: id, deleted: false})
        .exec((error, blogs) => {
            response.send(utils.createResult(error, blogs))
        })
})

router.post('/create', (request, response) => {
    const {title, body, author} = request.body
    const blog = new Blog()
    blog.title = title
    blog.author = author
    blog.body = body

    blog.save((error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/create-with-file', upload.single('file'), (request, response) => {
    const {title, body, author} = request.body
    const blog = new Blog()
    blog.title = title
    blog.author = author
    blog.body = body
    blog.file = request.file.filename

    blog.save((error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.put('/update/:id', (request, response) => {
    const id = request.params.id
    const {title, body, author} = request.body

    Blog.findOne({_id: id}, (error, blog) => {
        if (error) {
            response.send(utils.createResult('Blog doesnot exist'))
        } else {
            blog.title = title
            blog.body = body

            blog.save((error, result) => {
                response.send(utils.createResult(error, result))
            })
        }
    })
})


router.put('/share', (request, response) => {
    const {blogId, author} = request.body

    Blog.findOne({_id: blogId}, (error, blog) => {
        if (error) {
            response.send(utils.createResult('Blog doesnot exist'))
        } else {
            blog.sharedWith.push(author)
            blog.save((error, result) => {
                response.send(utils.createResult(error, result))
            })
        }
    })
})

router.put('/toggle-status/:id', (request, response) => {
    const id = request.params.id
    const {status} = request.body

    Blog.findOne({_id: id}, (error, blog) => {
        if (error) {
            response.send(utils.createResult('Blog doesnot exist'))
        } else {
            blog.public = status
        
            blog.save((error, result) => {
                response.send(utils.createResult(error, result))
            })
        }
    })
})

router.delete('/:id', (request, response) => {
    const id = request.params.id

    Blog.findOne({_id: id}, (error, blog) => {
        if (error) {
            response.send(utils.createResult('Blog doesnot exist'))
        } else {
            blog.deleted = true
        
            blog.save((error, result) => {
                response.send(utils.createResult(error, result))
            })
        }
    })
})



module.exports = router;
