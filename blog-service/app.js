const express = require('express')
const logger = require('morgan')
const mongoose = require('mongoose')
const dbConfig = require('./config/database')
const indexRouter = require('./routes/index')
const blogRouter = require('./routes/blogs') 

mongoose.connect(dbConfig, { useNewUrlParser: true, useCreateIndex: true })

const app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static( 'public'))

app.use('/', indexRouter)
app.use('/blog', blogRouter)

module.exports = app;
